import Link from "next/link";
import { ReactNode } from "react";
import Layout from "../components/Layout";

type MetaData = {
  size: number;
  mimetype: string;
  cacheControl: string;
};

type Images = {
  name: string;
  id: string;
  updated_at: string;
  created_at: string;
  last_accessed_at: string;
  metadata: MetaData;
  link: string;
};

type ImageGallery = Pick<Images, "link">;

const ImageGallery = ({ link }: ImageGallery) => {
  return (
    <div className="text-gray-600 body-font">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap -m-4">
          <div className="lg:w-1/3 sm:w-1/2 p-4">
            <div className="flex relative">
              <img
                src={link}
                alt="gallery"
                className="relative inset-0 w-3/4 h-2/4 object-cover object-center"
                strict-origin-when-cross-origin="true"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const ImagePage = ({ images }: { images: any }) => {
  return (
    <Layout>
      {images.length > 0 &&
        images.map((image: Images) => (
          <div key={image.id}>
            <ImageGallery {...image} />
          </div>
        ))}
    </Layout>
  );
};

export async function getStaticProps() {
  const res = await fetch(
    "https://qbosgatbzxoxqudcsznf.functions.supabase.co/cancun",
    {
      method: "POST",
      cache: "no-cache",
      headers: {
        Authorization:
          "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InFib3NnYXRienhveHF1ZGNzem5mIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NTI1NDM2NjMsImV4cCI6MTk2ODExOTY2M30.D0WONeM-sQQiVYKLRaihHJML2EozV8V7BQWjr4zwMyo",
        "Content-Type": "application/json",
      },
    }
  );
  const images = await res.json();
  return {
    props: {
      images,
    },
  };
}

export default ImagePage;

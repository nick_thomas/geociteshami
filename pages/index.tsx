import Link from "next/link";
import Layout from "../components/Layout";

const IndexPage = () => (
  <Layout>
    <h1>Welcome to the Nick & Hamsa Cancun Trip Website</h1>
    <p>
      <Link href="/about">
        <a>About</a>
      </Link>
    </p>
  </Layout>
);

export default IndexPage;

import Link from "next/link";
import Layout from "../components/Layout";

type NoteContent = {
  id: number;
  title: string;
};

type INotes = Array<NoteContent>;

const notes: INotes = [
  {
    id: 1,
    title: "Do a mobile first layout",
  },
  {
    id: 2,
    title: "Do a mobile first layout",
  },
  {
    id: 3,
    title: "Do d$a mobile first layout",
  },
];

const Notes = () => {
  return (
    <Layout>
      <div className="flex justify-center">
        <div className="rounded bg-gray-300 w-64 p-2">
          <div className="flex justify-between py-1">
            <h3 className="text-sm">New landing page</h3>
            <svg
              className="h-4 fill-current text-grey-dark cursor-pointer"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
            >
              <path d="M5 10a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4z" />
            </svg>
          </div>
          <div className="text-sm mt-2">
            {notes.map((note) => (
              <div
                key={note.id}
                className="bg-white p-2 rounded mt-1 border-b border-gray cursor-pointer hover:bg-gray-50"
              >
                {note.title}
              </div>
            ))}
          </div>
          <p className="mt-3 text-grey-dark">Add a card...</p>
        </div>
      </div>
    </Layout>
  );
};

export default Notes;

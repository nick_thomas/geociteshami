import Link from "next/link";
import Layout from "../components/Layout";

const AboutPage = () => (
  <Layout>
    <h1>About page</h1>
    <p>Nick Tom Thomas</p>
    <p>Working at Wavelo Engineering</p>
    <p> Full Stack Developer</p>
    <p>Hamsa Niranjan</p>
    <p>Currently working at Tucows</p>
    <p>Data Analyst</p>
  </Layout>
);

export default AboutPage;

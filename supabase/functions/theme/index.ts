// Follow this setup guide to integrate the Deno language server with your editor:
// https://deno.land/manual/getting_started/setup_your_environment
// This enables autocomplete, go to definition, etc.

import { serve } from "https://deno.land/std@0.131.0/http/server.ts";
import {
  has,
  propEq,
  isEmpty,
  not,
  ifElse,
} from "https://deno.land/x/ramda@v0.27.2/mod.ts";
import { createClient } from "https://esm.sh/@supabase/supabase-js@1.33.1";
import { corsHeaders } from "../__shared/cor.ts";
import { ThemeResponse } from "../../../interfaces/index.ts";

const getAssets = async (
  images: FileResponse[],
  supabase: any
): Promise<Array<ThemeResponse>> => {
  const response = await Promise.all(
    images.map(async (item) => {
      const { publicURL } = await supabase.storage
        .from("theme")
        .getPublicUrl(item.name);
      return { theme: publicURL };
    })
  );
  return response;
};

type FileResponse = {
  name: string;
};

const doesntExist = () => new Response("Theme doesn't exist", { status: 400 });

const generateTheme = async (req: any) => {
  const { theme } = await req.json();
  const SUPABASE_URL = Deno.env.get("SUPABASE_URL") ?? "";

  const SERVICE_ROLE_KEY = Deno.env.get("SUPABASE_SERVICE_ROLE_KEY") ?? "";

  if (not(isEmpty(SUPABASE_URL), isEmpty(SERVICE_ROLE_KEY))) {
    const supabase = createClient(SUPABASE_URL, SERVICE_ROLE_KEY);
    const { data, error } = await supabase.storage.from("theme").list();
    if (error) {
      return new Response("Something went wrong with themes", { status: 500 });
    }
    const checkIfThemeExists = data!.filter((item: FileResponse) =>
      item.name.includes(theme)
    );
    if (isEmpty(checkIfThemeExists)) return doesntExist();
    const response = await getAssets(checkIfThemeExists, supabase);
    return new Response(JSON.stringify(response), {
      headers: { ...corsHeaders, "Content-Type": "application/json" },
      status: 200,
    });
  } else {
    return new Response("Secrets not configured correctly", {
      headers: { ...corsHeaders, "Content-Type": "application/json" },
      status: 500,
    });
  }
};

const generateCors = () =>
  new Response("ok", {
    headers: corsHeaders,
  });

const hasOptions = propEq("method", "OPTIONS");

serve(async (req) => ifElse(hasOptions, generateCors, generateTheme)(req));

// To invoke:
// curl -i --location --request POST 'http://localhost:54321/functions/v1/' \
//   --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZS1kZW1vIiwicm9sZSI6ImFub24ifQ.625_WdcF3KHqz5amU0x2X5WWHP-OEs_4qj0ssLNHzTs' \
//   --header 'Content-Type: application/json' \
//   --data '{"name":"Functions"}'

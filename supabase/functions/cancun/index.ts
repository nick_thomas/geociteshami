// Follow this setup guide to integrate the Deno language server with your editor:
// https://deno.land/manual/getting_started/setup_your_environment
// This enables autocomplete, go to definition, etc.

import { serve } from "https://deno.land/std@0.131.0/http/server.ts";
import { createClient } from "https://esm.sh/@supabase/supabase-js@1.33.1";
import { isNil, isEmpty, not } from "https://deno.land/x/ramda@v0.27.2/mod.ts";

async function iterateList(item: any, supabase: any) {
  const { publicURL, error } = await supabase.storage
    .from("cancun-images")
    .getPublicUrl(item.name);
  if (error) return { ...item, link: "" };
  return { ...item, link: publicURL };
}

serve(async (req) => {
  const SUPABASE_URL = Deno.env.get("SUPABASE_URL") ?? "";

  const SERVICE_ROLE_KEY = Deno.env.get("SUPABASE_SERVICE_ROLE_KEY") ?? "";

  if (not(isEmpty(SUPABASE_URL), isEmpty(SERVICE_ROLE_KEY))) {
    const supabase = createClient(SUPABASE_URL, SERVICE_ROLE_KEY);

    const { data, error } = await supabase.storage
      .from("cancun-images")
      .list()!;
    if (error) return new Response("Forbidden", { status: 403 });
    if (isNil(data)) return new Response(JSON.stringify(data), { status: 200 });

    const response = await Promise.all(
      data!.map((item) => iterateList(item, supabase))
    );
    return new Response(JSON.stringify(response), {
      headers: { "content-type": "application/json" },
    });
  } else {
    return new Response("Secrets not configured correctly", { status: 500 });
  }
});

// To invoke:
// curl -i --location --request POST 'http://localhost:54321/functions/v1/' \
//   --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZS1kZW1vIiwicm9sZSI6ImFub24ifQ.625_WdcF3KHqz5amU0x2X5WWHP-OEs_4qj0ssLNHzTs' \
//   --header 'Content-Type: application/json' \
//   --data '{"name":"Functions"}'

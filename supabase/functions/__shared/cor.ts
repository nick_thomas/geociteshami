// For cors have to invoke it for each function call https://github.com/supabase/supabase/issues/6267
export const corsHeaders: HeadersInit = {
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Headers":
    "authorization, x-client-info, apikey,Content-Type",
};

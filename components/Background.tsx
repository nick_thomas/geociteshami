import React, { ReactNode, useEffect, useState } from "react";
import * as R from "ramda";
import { ThemeResponse } from "../interfaces/index";

enum TimeOfTheDay {
  NotSet = "",
  Morning = "morning",
  Afternoon = "afternoon",
  Night = "evening",
}

const isMorning = R.allPass([R.lte(6), R.gt(12)]);
const isAfternoon = R.allPass([(R.lte(12), R.gte(18))]);

const calculateTimeZone = R.cond([
  [isMorning, R.always<TimeOfTheDay>(TimeOfTheDay.Morning)],
  [isAfternoon, R.always<TimeOfTheDay>(TimeOfTheDay.Afternoon)],
  [R.T, R.always<TimeOfTheDay>(TimeOfTheDay.Night)],
]);

const fetchBackGround = async (
  value: TimeOfTheDay
): Promise<Array<ThemeResponse>> => {
  const response = await fetch(
    "https://qbosgatbzxoxqudcsznf.functions.supabase.co/theme",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InFib3NnYXRienhveHF1ZGNzem5mIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NTI1NDM2NjMsImV4cCI6MTk2ODExOTY2M30.D0WONeM-sQQiVYKLRaihHJML2EozV8V7BQWjr4zwMyo",
      },
      body: JSON.stringify({ theme: value }),
    }
  );
  const convertToJson = await response.json();
  return convertToJson;
};

const fetchAndSetWallpaper = async (
  value: TimeOfTheDay
): Promise<ThemeResponse> => {
  const [mob, nonMob] = await fetchBackGround(value);
  const isMobile = R.flip(R.lte)(400);
  const getBackground = R.ifElse(isMobile, R.always(mob), R.always(nonMob));
  return getBackground(window.innerWidth);
};

type BackgroundState = {
  state: TimeOfTheDay;
};

type SetBackGround = BackgroundState & ThemeResponse;
type Props = {
  children: ReactNode;
};
const Background = ({ children }: Props) => {
  const [background, setBackground] = useState<SetBackGround>({
    state: TimeOfTheDay.NotSet,
    theme: "",
  });
  useEffect(() => {
    const currentHour = new Date().getHours();
    const dayValue = calculateTimeZone(currentHour);
    if (R.identical(background.state, TimeOfTheDay.NotSet)) {
      fetchAndSetWallpaper(dayValue).then((value) => {
        setBackground({
          state: dayValue,
          ...value,
        });
      });
    }
  }, []);
  return (
    <>
      {R.not(R.isNil(background.state)) && (
        <div
          style={{
            backgroundImage: `url(${background.theme})`,
          }}
          className="min-h-screen bg-cover bg-no-repeat"
        >
          {children}
        </div>
      )}
    </>
  );
};

export default Background;

import React, { ReactNode, useEffect, useState } from "react";
import Link from "next/link";
import Head from "next/head";
import Background from "./Background";
import Header from "./Header";

type Props = {
  children?: ReactNode;
  title?: string;
};

const Layout = ({ children, title = "🇲🇽Cancun Trip Life Less" }: Props) => {
  return (
    <Background>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Header />
      <main className="min-h-[80vh] p-5">{children}</main>
      <footer className="p-5">
        <span>I'm here to stay (Footer)</span>
      </footer>
    </Background>
  );
};

export default Layout;

import React, { useState } from "react";

// Check the timestamp index with the background
// if background is available pull it with local, check if background is for the right time stamp
// fetch background from api
export default function useLocalStorage() {
  const [localBg, setLocalBg] = useState<Array<string>>([]);

  function accessLocalStorage() {
    if (localStorage.getItem("background") !== undefined) {
      let background = localStorage.getItem("background") as string;
      setLocalBg((curItem) => [...curItem, background]);
    }
  }
  accessLocalStorage();
  return [localBg, setLocalBg];
}
